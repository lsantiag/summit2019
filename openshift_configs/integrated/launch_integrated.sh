#!/bin/bash
oc login https://master00.example.com:443 -u user1 -p r3dh4t1!
oc new-project rhsso
oc project rhsso
IDPROUTE=$(oc get route/secure-sso --no-headers | awk '{print $2}')
if [ -z "$IDPROUTE" ]; then
 echo "SSO route not found.  Make sure you stand up RHSSO first."
 exit 1
fi
curl -L -v -k  https://${IDPROUTE}/auth/realms/master/protocol/saml/descriptor -o /tmp/metadata.xml
cat /tmp/metadata.xml
oc create configmap metadata --from-file=/tmp/metadata.xml
oc create -f routes.yaml
oc create -f svc.yaml
oc create -f dc.yaml
