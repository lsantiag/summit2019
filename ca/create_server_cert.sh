#!/bin/bash
set -x
P="/opt/ca"
openssl req -newkey rsa:4096 -keyout ${1}.key -new -sha256 -nodes -days 3650 -subj "/C=US/ST=North Carolina/L=Raleigh/O=Red Hat/OU=Summit/CN=${1}" -reqexts SAN -config <(cat ${P}/openssl.cnf <(printf "[SAN]\nsubjectAltName=DNS:${1},DNS:www.${1}")) -out ${1}.csr 
openssl rsa -in ${1}.key -out ${1}-rsa.key 
chmod 600 ${1}*.key 
openssl ca -config openssl.cnf -extensions v3_server -days 3650 -notext -md sha256 -in ${1}.csr -out ${1}.crt -keyfile myca.key -cert myca.crt -batch
