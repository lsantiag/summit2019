# Sidecar that authentication

## Some Housekeeping

 - Password: r3dh4t1!
 - Docs:  [https://bit.ly/2udRqDk](https://bit.ly/2udRqDk)
 - Slides: [https://bit.ly/2UKVTJo](https://bit.ly/2UKVTJo)
 - Replace **REPL** in this document with the GUID you were assigned.

## Intro
Hi!

This document and lab will demonstrate how [Red Hat Single Sign-On Server](https://access.redhat.com/products/red-hat-single-sign-on), [OpenShift](https://www.openshift.com/) and some off-the-shelf software (or custom code if you prefer) can provide a well-supported, automatable and predictable authentication platform that can scale to meet any businesses' needs.  

### The problem:

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/problem.png)

Large organizations tend to have several teams developing different types of applications simultaneously.  An IAM team in the organization must work with these teams and the organization as a whole to ensure that data stays as secure as possible while also optimizing for end-user experience.  At a high level this generally involves making sure identity information is stored in a central location and that users don't have to login too many times throughout the day.

However, once you go a bit deeper than this 1000 foot overview things get complicated quickly....

Your marketing team is writing an application in PHP and chooses to use [SimpleSAMLphp](https://simplesamlphp.org/) to link up to your central authentication servers.  

Your finance team is using some java application that was written by an intern in 1997.  It is protected using mutual TLS authentication since its data is highly sensitive.  

Your DevOps team is duct-taping a Ruby application together using a framework that only supports the [One Login OIDC](https://github.com/onelogin/onelogin-oidc-ruby) for authentication purposes.  

**Why this situation is not good:**

 - Developers generally don't want to learn boring RFC style spec information *just* to add authentication to their app. Some will rise to the challenge and others will consider not using the central authentication servers so they can avoid the burden.
 - The IAM team is required to have extremely detailed knowledge of several authentication protocols as not all authentication libraries are created equal, the spec can be ambiguous and things can fail in bizarre ways at the worst times.
 - Every integration between an application and a central authentication system will require a non-trivial amount of collaboration between the application developer and the IAM team.  This is because there is no single, universal standard authentication implementation that is heavily tested and documented.  How a developer and the IAM team will configure SimpleSAMLphp and even some other SAML library may be completely different depending on the supported features of the libraries and the terminology the library uses for its options.
 - This opens both teams and the organization as a whole up to implementation inconsistencies.  Maybe AppA supports encrypted data transfer but AppB doesn't.
 - Bugs and security issues can run rampant.  Will that DevOps team notice and handle bugs appropriately when they are running the bleeding edge version of that OIDC library?  That version of tomcat the intern setup for mutual TLS authentication is probably susceptible to [Heartbleed](http://heartbleed.com/) and other things.


### The solution:

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/solution.png)

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/webapp_sidecar_diagram.png)

## The sidecar

Every application runs inside of OpenShift with an authentication sidecar in the same pod. What is a sidecar? A sidecar is simply a container that is added to a pod in order to provide some new functionality. The purpose of this type of sidecar is that it would be developed and maintained by the company's Identity and Access Management (IAM) team to act as a reverse proxy to the developer's application.  The developer must then inspect and trust the `HTTP Headers` coming into their application as their authentication source. The sidecar pattern makes this possible. If we ran a separate reverse proxy layer outside of the pod, we'd still have to find a way to secure the pod. By running the proxy inside the pod, the application can listen only on localhost, knowing it can trust its peer containers for authentication.

**Why this is great:**

 - Web developers no longer have to learn the intricate details of an authentication protocol in order to meet security standards.  The business can move faster.
 - Locally testing applications becomes easier.
 - Debugging headers and a known quantity authentication library is easier than debugging a slew of different libraries.
 - The uniformity of the authentication sidecar allows for integration automation.
 - The IAM team can ensure that the sidecar offers all the features that the organization wants and that it is up to date and secure at all times.  

## Present and future

Red Hat IT is currently using this methodology to secure OpenShift Cluster authentication itself!  We are also using a very similar approach to secure SAS and an older perl application that couldn't be rewritten easily.

As for the future, we are watching [Istio](https://istio.io/) carefully and are trying to determine if we should shift to using that.  We are also trying to decide how much Red Hat specific business logic we should add to the sidecar itself.  With a custom written sidecar, we could for example, only allow people to authenticate at certain times of the day or query additional internal APIs so that the applications will get data from the SSO servers and other systems without having to query them directly themselves.  

## Lab Setup

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/lab.png)

In this lab we will be launching [Red Hat Single Sign-On Server](https://access.redhat.com/products/red-hat-single-sign-on) and three example applications inside of [OpenShift](https://www.openshift.com/).  

One of these example applications will be protected by its own built-in authentication.  The other two will be protected by [Apache](https://www.apache.org/) + [mod_auth_mellon](https://github.com/Uninett/mod_auth_mellon) authentication sidecars.  These sidecars will hook into the Red Hat Single Sign-On Server for authentication via the SAML protocol.  After authentication is performed, the sidecars will act as reverse proxies for their custom web applications and send over user information via HTTP headers.  The custom web applications then consume this user information and make authorization decisions accordingly.

To keep things simple this lab will only demonstrate one web application code base and a single authentication protocol library in the sidecar.  Know however that the beauty of this solution is that applications can be written in any language and expect to get the same authentication benefits no matter what.  Also note that one could easily extend the sidecar to support Mutual TLS Authentication via [mod_ssl](https://httpd.apache.org/docs/current/mod/mod_ssl.html) or any number of other authentication protocols as security policy dictates.

To restate the above:  We are creating a universal authentication abstraction layer.  Developers can now freely write code knowing what user information will be passed to them and how to consume it. Additionally, this abstraction layer can be made to be so robust that a security team could change what authentication protocol an application is using without even needing to involve the application developers.

Let's take a quick peek at how this works behind the scenes:  

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/mellon.png)

This is the Apache + mod_auth_mellon config that gets information from a SAML assertion, converts it into headers and then hands that to the custom webapp.  Note that this reverse proxy approach often also adds extra security.


![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/code.png)

This is the java code that reads in and processes the headers from the sidecar.  Obviously, you would implement your application specific authorization rules here.



## Red Hat Single Sign-On Server

[Red Hat Single Sign-On Server](https://access.redhat.com/products/red-hat-single-sign-on)  is a standards compliant full featured and extendable SSO product based on Red Hat's JBoss middleware stack. It supports the SAML, OIDC and OAuth protocols. It can federate information with LDAP and kerberos user stores and it supports brokered identity flows for things like social auth. It comes with an easy to use GUI and an API for automation. The RHSSO team also provides adapters to make integrating with other internally written applications easier.

In short, if you already have all your employee information in a central place like LDAP or AD then you can have RHSSO use that data and act as the SSO bridge that will allow your employees to login to countless internal applications and vendors using a single set of credentials.

**Let's stand it up and login**

Use a shell script that logs into OpenShift and then creates a full infrastructure stack (route, service, pods, etc) using the stock Red Hat Single Sign-On Server template and some run-time parameters to customize it.

If you haven't SSH'd to your VM yet, <br/>
**please open a terminal on your laptop and run:**

>ssh cloud-user@workstation-**REPL**.rhpds.opentlc.com

And login using "r3dh4t1!" password.

If you've already SSH'd as another user (like lab-user), then do: <br/>
**(only run the command below if you are not already the cloud-user)**

>sudo su - cloud-user

And login using "r3dh4t1!" password.

Then we can run the launch script like so:

> cd ~/summit2019/openshift_configs/sso<br/>
> ./launch_sso.sh

Before we go to the RHSSO web console we need to trust a cert.

1.  Click <a href="https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/ca/ravello_openshift_ca.crt" target="_blank">here</a>

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/ca_page.png)

2. Right click the page > Save Page As.
3. Choose your home folder, name the file CA.crt, and click save.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/save.png)

4. Click the three horizontal lines to the right of Firefox's address bar and choose preferences

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/ff_preferences.png)

5. Start typing the work "certificate" in the search box and then click "View Certificates..."

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/view_ceritificates.png)

6. Click Authorities.  Click Import

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/import.png)


7. Click your home folder. Choose the CA.crt. Click open.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/choose_cert.png)

8. <b>Check all the boxes and Click Ok.  </b>

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/check_all.png)

9. Click ok in the "certificate manager" window

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/cert_manager_ok.png)
<br/>
<br/>
<div align="center"> <b> Please Pause here before continuing to the next section of this lab, we will let you know when to continue. </b> </div>
<br/>
<div align="center"> <img width="300" height="300" src="https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/pause.png"> </div>
<br/>
<br/>
<b>In the meantime, while we give the others a few minutes to catch up with everyone else, lets take a deeper look on the Openshift side of things by trying out a few commands on your VM! </b><br/>
"oc whoami"  -- shows the current user <br/>
"oc project" -- shows the current project <br/>
"oc status" -- shows overview of the current project <br/>
"oc get pods" -- shows list of pods running for the project <br/>
"oc get services" -- shows a list of services running for the project <br/>
"oc get dc" -- shows a lists of DC's running for the project <br/>
"oc get route" -- shows a list of routes running for the project <br/>
"oc get events" -- shows overview of events for the project <br/>
"oc get all" -- shows overview of the project <br/>

<br/>
<br/>
<br/>
<br/>
<b> Let's explore a bit </b>

Go to `https://secure-sso-rhsso.apps-`**`REPL`**`.generic.opentlc.com/`  in your browser.  You should not see any certificate errors and you should be greeted by the Red Hat Single Sign-On Server page.    

Click on `Administration Console` and Login using `admin` and `r3dh4t1!` password.

`Realms`  are logical collections of items and can have separate configuration options and access permissions.

`Clients`  are the properties you have SSO integrated with. Each client can have their own unique settings.

`Roles`  are what you would imagine. You can think of them as groups a person is in. You can pass these to an SSO integrated property so they can make their own authorization calls based on that.

`Identity Providers`  are for brokering auth and linking identities from different central sources of authority. Social and other complex authentication flows fall under this.

`Mappers`  translate data from a  `User Federation`  source into something accessible and usable by RHSSO.

`User Federations`  are the backends RHSSO hooks into to get user information and to do authentication against.

`Authentication`  Allows you do to fine grained PAM like module stacking and directing of actions.

The rest of the things do what you would imagine they would. RHSSO is very extendable and in one of our environments we have defined our own  `User Federation`  SPIs to talk to a custom database and have defined custom  `Mappers`  to let us use data more flexibly.

Before we setup and SSO enable some clients, let's add a user:

1.  In the left menu, under Manage, go to Users then click Add User

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/add_user_pre.png)

2. The add user page loads.  Fill it in like so.

    1.  Username: testuser
    2.  Email:  testuser@example.com
    3.  First Name: Test
    4.  Last Name: User
    5.  Enabled
    6.  Email verified off
    7.  No actions
    8.  Save

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/add_user.png)

3.  The users page loads
    1.  Click the credentials tab
    2.  New Password: r3dh4t1!
    3.  Password Confirmation: r3dh4t1!
    4.  Temporary: off
    5.  Click the Reset Password button

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/reset_password.png)

4.   Click Change Password in the pop up

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/change_password.png)

<br/>
<br/>
<div align="center"> <b> Please Pause here before continuing to the next section of this lab, we will let you know when to continue. </b> </div>
<br/>
<div align="center"> <img width="300" height="300" src="https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/pause.png"> </div>
<br/>
<br/>
<b>In the meantime, while we give the others a few minutes to catch up with everyone else, lets take a deeper look on the Openshift side of things by trying out a few commands on your VM! </b><br/>
"oc whoami"  -- shows the current user <br/>
"oc project" -- shows the current project <br/>
"oc status" -- shows overview of the current project <br/>
"oc get pods" -- shows list of pods running for the project <br/>
"oc get services" -- shows a list of services running for the project <br/>
"oc get dc" -- shows a lists of DC's running for the project <br/>
"oc get route" -- shows a list of routes running for the project <br/>
"oc get events" -- shows overview of events for the project <br/>
"oc get all" -- shows overview of the project <br/>

<br/>
<br/>
<br/>
<br/>

## Example Applications

These are simple [Spring Boot](https://spring.io/projects/spring-boot) applications.  As mentioned in the `Lab Setup` section, one of these will be using built-in authentication and the other two will use HTTP Headers from their sidecars for authentication.  

**Let's stand them up**

Use a shell script that replaces some OpenShift template parameters with your unique lab hostname.
**Run this on your terminal and please make sure you replace REPL with your unique ID for this script to work correctly**

>cd ~/summit2019/openshift_configs/integrated<br/>
>./text_replace.sh apps-**REPL**.generic.opentlc.com

Use a shell script that will download the SAML IDP metadata from the Red Hat Single Sign-On Server and store it as a ConfigMap object in OpenShift.  Then, it will create the infrastructure in OpenShift by applying the template files.

>./launch_integrated.sh


**Let's explore a bit**

Open a browser and go to `http://base-webapp-rhsso.apps-`**`REPL`**`.generic.opentlc.com`

If you don't see a login page yet, it means the app hasn't quite finished deploying. Wait a minute and hit refresh in your browser.

This is the web app with built-in authentication.   Login using `admin` and the password: r3dh4t1!
Imagine you had several of these and you had to login to each of them.

We have an application running with the sidecar too, but we haven't finished setting it up yet. You will see an error if you go to `https://integrated1-rhsso.apps-`**`REPL`**`.generic.opentlc.com`. Go ahead and point your browser to that now.  You should get bounced to an error page at `https://secure-sso-rhsso.apps-`**`REPL`**`.generic.opentlc.com`.  What happened here is the sidecar authentication proxy caught your request and sent over a SAML based login request to the Red Hat Single Sign-On Server.  Since the Red Hat Single Sign-On Server doesn't know about this application yet, it shows this error.  

<br/>
<br/>
<div align="center"> <b> Please Pause here before continuing to the next section of this lab, we will let you know when to continue. </b> </div>
<br/>
<div align="center"> <img width="300" height="300" src="https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/pause.png"> </div>
<br/>
<br/>
<b>In the meantime, while we give the others a few minutes to catch up with everyone else, lets take a deeper look on the Openshift side of things by trying out a few commands on your VM! </b><br/>
"oc whoami"  -- shows the current user <br/>
"oc project" -- shows the current project <br/>
"oc status" -- shows overview of the current project <br/>
"oc get pods" -- shows list of pods running for the project <br/>
"oc get services" -- shows a list of services running for the project <br/>
"oc get dc" -- shows a lists of DC's running for the project <br/>
"oc get route" -- shows a list of routes running for the project <br/>
"oc get events" -- shows overview of events for the project <br/>
"oc get all" -- shows overview of the project <br/>

<br/>
<br/>
<br/>
<br/>


## SSO enabling the example applications
Let's hook those example applications up to the Red Hat Single Sign-On Server.  

As a reminder, what makes this sidecar approach so great is that the steps we do next will work perfectly for _any_ application using the sidecar.  

The first thing we need to do is download the SAML metadata from our sample applications.<br/>
**Open a new terminal on your laptop (do not run this on the VM) and do the following:**

>curl -L -v -k https://integrated1-rhsso.apps-<b>REPL</b>.generic.opentlc.com/secret/endpoint/metadata -o integrated1.xml

Now we need to import this data into the Red Hat Single Sign-On Server and choose what data we want to send over to these clients.  

 1. Go to `https://secure-sso-rhsso.apps-`**`REPL`**`.generic.opentlc.com/auth/admin/`  in your browser
 2. Login using admin and the password r3dh4t1! if needed
 3. Click on Clients and Create

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/clients_create.png)

4. Click Select File

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/select_file.png)

5. Browse to your home directory and choose integrated1.xml and click open

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/integrated.png)

6. The Add Client page will have data populated now.  Click Save

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/add_client_save.png)

7. The client page loads.  Make the following changes:

	1. Disable "Encrypt Assertions"
	2. Disable "Client Signature Required"
	3. Enable "Force Name ID Format"
	4. Set Name ID Format to `username`
	5. Click Save

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/client_config.png)

8. Click on the Client Scopes tab.  

	1. Click on `role_list` under `Assigned Default Client Scopes`
	2. Click the `Remove Selected` button.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/client_scope.png)


9. Click on the Mappers tab

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/mappers_click.png)

10. Click the Add Builtin button

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/add_builtin_click.png)

11. Tick the boxes for "X500 email"  "X500 givenName" and "X500 surname"  and then click the "Add selected" button.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/builtin_choices.png)

12. The Mappers tab loads again.  Click the Create button

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/create_click.png)

13. The Create Protocol Mapper screen loads.  Fill in the following:

	1. Protocol: saml
	2. Name: groups
	3. Mapper Type: Role List
	4. Role Attribute Name: groups
	5. Friendly Name: groups
	6. SAML Attribute NameFormat: Basic
	7. Single Role Attribute: Enabled
	8. Click Save

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/groups_mapper.png)

14. Click on Clients and Export next to the `https://integrated1-rhsso.apps-`**`REPL`**`.generic.opentlc.com/secret/endpoint/metadata` client

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/client_export.png)


15. Choose Save File and click Ok

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/client_save.png)

16. You are still on the Clients page.  Click create.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/clients_create.png)

17. You are on the Add Client Page.  Click Select File.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/select_file.png)

18. Browse to your home Downloads Folder choose the `https___integrated1-rhsso.apps-`**`REPL`**`.generic.opentlc.com_secret_endpoint_metadata.json` file and click Open

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/export_import.png)

19. The Add Client page will have data populated now.  Change `integrated1` to `integrated2` in the Client ID box and click Save.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/add_client_save2.png)

20. The client page loads.  Change `integrated1` to `integrated2` in all of the following places and then click Save

	1. Valid Redirect URIs
	2. Assertion Consumer Service POST Binding URL
	3. Logout Service Redirect Binding URL

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/integrated2.png)


<br/>
<br/>
<div align="center"> <b> Please Pause here before continuing to the next section of this lab, we will let you know when to continue. </b> </div>
<br/>
<div align="center"> <img width="300" height="300" src="https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/pause.png"> </div>
<br/>
<br/>
<b>In the meantime, while we give the others a few minutes to catch up with everyone else, lets take a deeper look on the Openshift side of things by trying out a few commands on your VM! </b><br/>
"oc whoami"  -- shows the current user <br/>
"oc project" -- shows the current project <br/>
"oc status" -- shows overview of the current project <br/>
"oc get pods" -- shows list of pods running for the project <br/>
"oc get services" -- shows a list of services running for the project <br/>
"oc get dc" -- shows a lists of DC's running for the project <br/>
"oc get route" -- shows a list of routes running for the project <br/>
"oc get events" -- shows overview of events for the project <br/>
"oc get all" -- shows overview of the project <br/>

<br/>
<br/>
<br/>
<br/>

## Testing the sidecar SSO + header auth setup

Click the three horizontal lines to the right of Firefox's address bar and choose "New Private Window"

NOTE: You are doing this because your current browser is logged in as "admin" and we want to login as the testuser we created.

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/new_private_window.png)


Go to `https://integrated1-rhsso.apps-`**`REPL`**`.generic.opentlc.com` in your private window.  It should send you to the Red Hat Single Sign-On Server.   Login using `testuser` and the password r3dh4t1!

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/integrated_login.png)

You will now be directed back to the example integrated1 application where you will see information about this user on the webpage.  Behind the scenes the sidecar did the SAML work then stuffed information about  that user into headers.  Then the custom Spring Boot application read this data in, allowed you access, and printed it to the page you are seeing.  

![enter image description here](https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/integrated1_done.png)

Next, go to `https://integrated2-rhsso.apps-`**`REPL`**`.generic.opentlc.com` in your private window.  If you watch the status bar closely (or open up developer tools) you will see that it bounces you through the Red Hat Single Sign-On Server and then back to the integrated2 application where it prints out your information.  

You can now access many applications while only logging in once.  The developers of those applications only needed basic logic in their code and to use the authentication sidecar to accomplish this.  The IAM team knows that things are secure and can automate new application onboarding.  Everybody is happy and this is a huge win across the board.

<br/>
<br/>
<div align="center"> <b> Please Pause here before continuing to the next section of this lab, we will let you know when to continue. </b> </div>
<br/>
<div align="center"> <img width="300" height="300" src="https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/docs/pause.png"> </div>
<br/>
<br/>
<b>In the meantime, while we give the others a few minutes to catch up with everyone else, lets take a deeper look on the Openshift side of things by trying out a few commands on your VM! </b><br/>
"oc whoami"  -- shows the current user <br/>
"oc project" -- shows the current project <br/>
"oc status" -- shows overview of the current project <br/>
"oc get pods" -- shows list of pods running for the project <br/>
"oc get services" -- shows a list of services running for the project <br/>
"oc get dc" -- shows a lists of DC's running for the project <br/>
"oc get route" -- shows a list of routes running for the project <br/>
"oc get events" -- shows overview of events for the project <br/>
"oc get all" -- shows overview of the project <br/>

<br/>
<br/>
<br/>
<br/>

## BONUS! Adding sidecar SSO integration to the base-webapp

We've seen how the architecture works, and how to configure SSO with a SAML enabled sidecar. The value of this approach is how little the integrated application has to change or know about its companion sidecar. To demonstrate this, let's now update the "base-webapp", that was not integrated with SSO, with the sidecar to add SSO integration.

To do this, we're going to need to:

1. Modify our base-webapp's OpenShift configuration to enable header-based auth using a Spring profile that we've already added to the application
2. Update our `DeploymentConfig` for the base-webapp to add the sidecar, which will expose another port from the `Pod`.
3. Add new secured `Service` and `Route` that handles HTTPS traffic and routes it to the new secured sidecar endpoint
4. Add the additional client configuration to SSO as before

Our OpenShift objects for the webapps were set up back under the [Example Applications](#example-applications) section. We'll be editing the YAML files that were used by the `launch_integrated.sh` script.

> cd ~/summit2019/openshift_configs/integrated<br>
> vim dc.yaml

**This section assumes you are comfortable with a terminal text editor like vim. If you are not comfortable with a text editor, you can download the necessary updates** by running

> `# STOP: Only do this if you'd rather download pre-edited configs than edit them manually`<br>
> cd ~/summit2019/openshift_configs/integrated<br>
> wget https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/openshift_configs/integrated-bonus/dc.yaml -O dc.yaml<br>
> wget https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/openshift_configs/integrated-bonus/routes.yaml -O routes.yaml<br>
> wget https://gitlab.com/redhatsummitlabs/red-hat-it-features-sidecar-that-authentication/raw/master/openshift_configs/integrated-bonus/svc.yaml -O svc.yaml<br>
> ./text_replace.sh apps-**REPL**.generic.opentlc.com
> cat dc.yaml

1. Find the "base-webapp" `DeploymentConfig` at the top of the dc.yaml file. Under `spec.template.spec.containers`, find where the base-webapp container's environment variables are set, which will look like the following if you haven't already downloaded the updated config:

   ```yaml
           name: base-webapp
           env:
             - name: JAVA_TOOL_OPTIONS
               value: '-Xmx256m'
   ```

   If you are editing the file, change this to:

   ```yaml
           name: base-webapp
           env:
             - name: JAVA_TOOL_OPTIONS
               value: '-Xmx256m -Dspring.profiles.active=sidecar-auth'
   ```

   This configures our application to authenticate based on headers, instead of managing sessions and users itself.

2. Under `spec.template.spec.containers`, add (or review, if you've already downloaded the updates) the commented section below:

   ```yaml
               memory: 256Mi
           terminationMessagePath: /dev/termination-log
           terminationMessagePolicy: File
         # BEGIN EDIT ------
         - name: sidecar
           image: docker-registry.default.svc:5000/summitbuilds/saml:latest
           imagePullPolicy: IfNotPresent
           ports:                                                                                                                       
           - containerPort: 8443                                                                                                        
             protocol: TCP                                                                                                              
           volumeMounts:                                                                                                                
           - mountPath: "/etc/config"                                                                                                   
             name: config-volume                                                                                                        
             readOnly: true                                                                                                             
           - mountPath: "/etc/cert-volume"                                                                                              
             name: sidecar-cert-volume                                                                                                  
             readOnly: true                                                                                                             
           resources:                                                                                                                   
             limits:                                                                                                                    
               cpu: '500m'                                                                                                              
               memory: 512Mi                                                                                                            
             requests:                                                                                                                  
               cpu: '100m'                                                                                                              
               memory: 256Mi                                                                                                            
           env:                                                                                                                         
           - name: SERVERNAME                                                                                                           
             value: "https://integrated3-rhsso.apps-__REPL_REPLACE_WITH_GUID__.generic.opentlc.com:443"
         # END EDIT ------
         dnsPolicy: ClusterFirst
         restartPolicy: Always
   ```

3. **[IF EDITING]** IMPORTANT! **Replace `__REPL_REPLACE_WITH_GUID__` with your lab GUID.**
4. Add or review the referenced volumes to `spec.template.spec.volumes` field as shown below in the commented section:

   ```yaml
             requests:                                                                                                                  
               cpu: '100m'                                                                                                              
               memory: 256Mi                                                                                                            
           env:                                                                                                                         
           - name: SERVERNAME                                                                                                           
             value: "https://integrated3-rhsso.apps-__REPL_REPLACE_WITH_GUID__.generic.opentlc.com:443"
         # BEGIN EDIT ------
         volumes:                                                                                                                         
         - name: config-volume                                                                                                            
           configMap:                                                                                                                     
             defaultMode: 420                                                                                                             
             name: metadata                                                                                                               
         - name: sidecar-cert-volume                                                                                                      
           secret:                                                                                                                        
             secretName: integrated3-cert
         # END EDIT ------
         dnsPolicy: ClusterFirst
   ```

5. **[IF EDITING]** Save and exit out of vim via typing `:x` and hitting `enter`. Then, edit svc.yaml via running `vim svc.yaml` in your workstation terminal.<br>
   **[IF DOWNLOADED UPDATES]** Run `cat svc.yaml` in your workstation terminal to review the updates.
6. Add or review the following at the **end of the file**:

   ```yaml
   ---
   kind: Service
   apiVersion: v1
   metadata:
     name: integrated3
     creationTimestamp:
     labels:
       run: integrated3
     annotations:
       openshift.io/generated-by: OpenShiftNewApp
       description: The web server's https port.
       service.alpha.openshift.io/serving-cert-secret-name: integrated3-cert
   spec:
     ports:
     - name: 8443-tcp
       protocol: TCP
       port: 8443
       targetPort: 8443
     - name: 8080-tcp
       protocol: TCP
       port: 8080
       targetPort: 8080
     selector:
       # This is base-webapp instead of integrated3 because it must match our base-webapp DeploymentConfig that we're modifying
      run: base-webapp
   ```
7. **[IF EDITING]** Save and exit out of vim via typing `:x` and hitting `enter`. Then, edit routes.yaml via running `vim routes.yaml` in your workstation terminal.<br>
   **[IF DOWNLOADED UPDATES]** Run `cat routes.yaml` in your terminal to review the updates.
8. Add or review the following at the **end of the file**:

   ```yaml
   ---
   kind: Route
   apiVersion: v1
   metadata:
     name: integrated3
     creationTimestamp:
     labels:
       run: integrated3
     annotations:
       description: Route for application's https service.
       openshift.io/host.generated: 'true'
   spec:
     to:
       name: integrated3
     port:
       targetPort: 8443
     tls:
       termination: reencrypt
       insecureEdgeTerminationPolicy: Redirect
   ```
9. Now that we've gone through all of the updates, let's apply them by running in your workstation terminal:

    > oc apply -R -f ./

10. We're done updating the application, but we'll still see an error if you go to `https://integrated3-rhsso.apps-`**`REPL`**`.generic.opentlc.com/` in your laptop's browser. Open that link now. We need to set up the new client.

11. Follow the steps above under [SSO enabling the applications](#sso-enabling-the-example-applications) starting from step **16** and instead of replacing `integrated1` with `integrated2`, replace it with `integrated3`.

12. Now open again `https://integrated3-rhsso.apps-`**`REPL`**`.generic.opentlc.com/` in your laptop's private browser window that was logged in as `testuser`. You should now be logged in, and can bounce between all of integrated1, integrated2, and integrated3 applications freely without logging in.  

## Closing remarks
Please rate this lab!

Click `RESET STATION` in your browser and logout of your laptop before you leave.
